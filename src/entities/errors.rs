use std::sync::PoisonError;

#[derive(Debug, PartialEq, Eq)]
pub enum DbError {
    CanNotConnect(String),
    CanNotInitTable(&'static str, String),
    NotFound(Option<String>),
    MutexError,
    DeserializationError(String),
    Other(String),
}

pub type DbResult<T> = Result<T, DbError>;

impl<T> From<PoisonError<T>> for DbError {
    fn from(_: PoisonError<T>) -> Self {
        DbError::MutexError
    }
}

impl From<rusqlite::Error> for DbError {
    fn from(src: rusqlite::Error) -> Self {
        DbError::Other(src.to_string())
    }
}
