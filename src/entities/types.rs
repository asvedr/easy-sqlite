pub type SStr = &'static str;
pub type SVec<T> = &'static [T];
