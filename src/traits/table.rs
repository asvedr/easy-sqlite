use crate::entities::types::{SStr, SVec};

pub trait ITable {
    // const for FK
    const PK: SStr = "";
    // table name
    const NAME: SStr;
    // [(column name, column type)]
    const COLUMNS: SVec<(SStr, SStr)>;
    // [column name]
    const INDEXES: SVec<SStr> = &[];
    // [column name]
    const UNIQUE: SVec<SStr> = &[];
    // [(self column, ext table, ext column)]
    const FOREIGN_KEYS: SVec<(SStr, SStr, SStr)> = &[];

    fn get_columns_as_str(exclude: &[&str]) -> String {
        Self::COLUMNS
            .iter()
            .filter(|(c, _)| !exclude.contains(c))
            .map(|(c, _)| format!("`{}`", c))
            .collect::<Vec<_>>()
            .join(",")
    }
}
