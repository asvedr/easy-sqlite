pub mod entities;
mod impls;
pub mod traits;
mod utils;

pub use entities::errors;
pub use entities::types::{SStr, SVec};
pub use impls::mutex_connection::MSQLConnection;
pub use impls::rc_connection::RSQLConnection;
pub use impls::table_manager::TableManager;
pub use traits::repo::{IDbRepo, IExecutor, INewDbRepo};
pub use traits::table::ITable;
pub use utils::{deescape, escape};
