use std::mem;
use std::path::Path;
use std::sync::{Arc, Mutex, MutexGuard};

use rusqlite::{Connection, Row, ToSql};

use crate::entities::errors::{DbError, DbResult};
use crate::impls::executor::Executor;
use crate::traits::repo::{IConnection, IExecutor};

#[derive(Clone)]
pub struct MSQLConnection {
    inner: Arc<Mutex<Connection>>,
}

#[derive(Clone)]
pub struct Lock {
    mtx: Arc<Mutex<Connection>>,
    guard: Option<Arc<MutexGuard<'static, Connection>>>,
}

impl Drop for Lock {
    fn drop(&mut self) {
        mem::replace(&mut self.guard, None);
    }
}

impl MSQLConnection {
    pub fn new<P: AsRef<Path>>(path: P) -> DbResult<Self> {
        let cnct = Connection::open(path).map_err(|err| DbError::CanNotConnect(err.to_string()))?;
        Ok(Self {
            inner: Arc::new(Mutex::new(cnct)),
        })
    }
}

impl IConnection for MSQLConnection {
    type Locked = Lock;

    fn lock(&self) -> DbResult<Self::Locked> {
        let mtx = self.inner.clone();
        let guard = self.inner.lock()?;
        let guard: MutexGuard<'static, Connection> = unsafe { mem::transmute(guard) };
        Ok(Lock {
            mtx,
            guard: Some(Arc::new(guard)),
        })
    }

    fn with<T, F: FnOnce(&Connection) -> DbResult<T>>(&self, fun: F) -> DbResult<T> {
        let guard = self.inner.lock()?;
        fun(&guard)
    }
}

impl IExecutor for MSQLConnection {
    type Locked = Lock;

    fn lock(&self) -> DbResult<Self::Locked> {
        <MSQLConnection as IConnection>::lock(self)
    }

    fn get_one<T, F: FnMut(&Row<'_>) -> rusqlite::Result<T>>(
        &self,
        query: &str,
        params: &[&dyn ToSql],
        serializer: F,
    ) -> DbResult<T> {
        Executor::new(self).get_one(query, params, serializer)
    }

    fn get_many<T, F: FnMut(&Row<'_>) -> rusqlite::Result<T>>(
        &self,
        query: &str,
        params: &[&dyn ToSql],
        serializer: F,
    ) -> DbResult<Vec<T>> {
        Executor::new(self).get_many(query, params, serializer)
    }

    fn execute(&self, query: &str, params: &[&dyn ToSql]) -> DbResult<()> {
        Executor::new(self).execute(query, params)
    }

    fn execute_return_id(&self, query: &str, params: &[&dyn ToSql]) -> DbResult<i64> {
        Executor::new(self).execute_return_id(query, params)
    }
}

impl IConnection for Lock {
    type Locked = Self;

    fn lock(&self) -> DbResult<Self::Locked> {
        Ok(Self {
            mtx: self.mtx.clone(),
            guard: self.guard.clone(),
        })
    }

    fn with<T, F: FnOnce(&Connection) -> DbResult<T>>(&self, fun: F) -> DbResult<T> {
        if let Some(ref grd) = self.guard {
            fun(grd)
        } else {
            unreachable!()
        }
    }
}

impl IExecutor for Lock {
    type Locked = Self;

    fn lock(&self) -> DbResult<Self::Locked> {
        <Lock as IConnection>::lock(self)
    }

    fn get_one<T, F: FnMut(&Row<'_>) -> rusqlite::Result<T>>(
        &self,
        query: &str,
        params: &[&dyn ToSql],
        serializer: F,
    ) -> DbResult<T> {
        Executor::new(self).get_one(query, params, serializer)
    }

    fn get_many<T, F: FnMut(&Row<'_>) -> rusqlite::Result<T>>(
        &self,
        query: &str,
        params: &[&dyn ToSql],
        serializer: F,
    ) -> DbResult<Vec<T>> {
        Executor::new(self).get_many(query, params, serializer)
    }

    fn execute(&self, query: &str, params: &[&dyn ToSql]) -> DbResult<()> {
        Executor::new(self).execute(query, params)
    }

    fn execute_return_id(&self, query: &str, params: &[&dyn ToSql]) -> DbResult<i64> {
        Executor::new(self).execute_return_id(query, params)
    }
}
