use rusqlite::{Connection, ToSql};

use crate::errors::{DbError, DbResult};
use crate::traits::repo::{IConnection, IExecutor};

pub struct Executor<Cnn: IConnection> {
    cnn: Cnn,
}

impl<Cnn: IConnection> Executor<Cnn> {
    #[inline]
    pub fn new(cnn: Cnn) -> Self {
        Self { cnn }
    }
}

impl<Cnn: IConnection> IExecutor for Executor<Cnn> {
    type Locked = Executor<Cnn::Locked>;

    fn lock(&self) -> DbResult<Self::Locked> {
        Ok(Executor::new(self.cnn.lock()?))
    }

    fn get_one<T, F: FnMut(&rusqlite::Row<'_>) -> rusqlite::Result<T>>(
        &self,
        query: &str,
        params: &[&dyn ToSql],
        serializer: F,
    ) -> DbResult<T> {
        let fun = move |cnn: &Connection| -> DbResult<T> {
            let val = cnn.prepare(query)?.query_map(params, serializer)?.next();
            match val {
                Some(val) => Ok(val?),
                None => Err(DbError::NotFound(None)),
            }
        };
        self.cnn.with(fun)
    }

    fn get_many<T, F: FnMut(&rusqlite::Row<'_>) -> rusqlite::Result<T>>(
        &self,
        query: &str,
        params: &[&dyn ToSql],
        serializer: F,
    ) -> DbResult<Vec<T>> {
        let fun = move |cnn: &Connection| -> DbResult<Vec<T>> {
            let val = cnn
                .prepare(query)?
                .query_map(params, serializer)?
                .collect::<Result<Vec<T>, _>>()?;
            Ok(val)
        };
        self.cnn.with(fun)
    }

    fn execute(&self, query: &str, params: &[&dyn ToSql]) -> DbResult<()> {
        self.cnn.with(move |cnn| {
            cnn.execute(query, params)?;
            Ok(())
        })
    }

    fn execute_return_id(&self, query: &str, params: &[&dyn ToSql]) -> DbResult<i64> {
        self.cnn.with(move |cnn| {
            cnn.execute(query, params)?;
            Ok(cnn.last_insert_rowid())
        })
    }
}
