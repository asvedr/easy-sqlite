use std::path::Path;
use std::rc::Rc;

use rusqlite;
use rusqlite::{Connection, Row, ToSql};

use crate::entities::errors::{DbError, DbResult};
use crate::impls::executor::Executor;
use crate::traits::repo::{IConnection, IExecutor};

#[derive(Clone)]
pub struct RSQLConnection {
    inner: Rc<rusqlite::Connection>,
}

impl RSQLConnection {
    pub fn new<P: AsRef<Path>>(path: P) -> DbResult<Self> {
        let cnct = rusqlite::Connection::open(path)
            .map_err(|err| DbError::CanNotConnect(err.to_string()))?;
        Ok(Self {
            inner: Rc::new(cnct),
        })
    }
}

impl IConnection for RSQLConnection {
    type Locked = Self;

    fn lock(&self) -> DbResult<Self::Locked> {
        panic!("Lock is not allowed for RC connection. Use mutex")
    }

    fn with<T, F: FnOnce(&Connection) -> DbResult<T>>(&self, fun: F) -> DbResult<T> {
        fun(&self.inner)
    }
}

impl IExecutor for RSQLConnection {
    type Locked = Self;

    fn lock(&self) -> DbResult<Self::Locked> {
        panic!("Lock is not allowed for RC connection. Use mutex")
    }

    fn get_one<T, F: FnMut(&Row<'_>) -> rusqlite::Result<T>>(
        &self,
        query: &str,
        params: &[&dyn ToSql],
        serializer: F,
    ) -> DbResult<T> {
        Executor::new(self).get_one(query, params, serializer)
    }

    fn get_many<T, F: FnMut(&Row<'_>) -> rusqlite::Result<T>>(
        &self,
        query: &str,
        params: &[&dyn ToSql],
        serializer: F,
    ) -> DbResult<Vec<T>> {
        Executor::new(self).get_many(query, params, serializer)
    }

    fn execute(&self, query: &str, params: &[&dyn ToSql]) -> DbResult<()> {
        Executor::new(self).execute(query, params)
    }

    fn execute_return_id(&self, query: &str, params: &[&dyn ToSql]) -> DbResult<i64> {
        Executor::new(self).execute_return_id(query, params)
    }
}
